import React, {useEffect, useState} from 'react';
import { Collapse } from 'antd';
import axios from 'axios';


const Collapsible = ()=>{
    const { Panel } = Collapse;
    const [faqData, SetfaqData] = useState([]);
    const [fetchFaq, setFetchFaq] =useState(false);
    const [errmsg, setErrmsg] =useState("");

    useEffect(() => {
        let FaqUrl = "https://sheetsu.com/apis/v1.0su/9260d24dff8f";
        // let FaqUrl = "https://sheetsu.com/apis/v1.0bu/9abdd5b3fe24";
        const content = async() =>{
            try{
                const response = await axios.get(FaqUrl);
                const data = await response.data
                SetfaqData(data);
                setFetchFaq(true);
            }catch (error) {
                setErrmsg(error.message);
                setFetchFaq(true);

            }
        }
        content();
    },[]);
    //page loader
    const spinner = <div className = "center_spinner">
                        <div id="main">
                            <span className="spinner"></span>  
                        </div>
                    </div>;
    //data from the api goes into collapsible
    const collapse =  <Collapse accordion defaultActiveKey={['1']} expandIconPosition="right">
            {faqData.map((data,index)=>(
                    <Panel header={data.title} key={data.index}>
                        <p>{data.content}</p>
                    </Panel>
                ))}
            </Collapse>
    return(
        <div className="collapsible_con">
            {/* call the functions  */}
            <span className="errmsg">{errmsg}</span>
           {fetchFaq ? collapse : spinner}
        </div>
        
    )
}
export default Collapsible;