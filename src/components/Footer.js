import React from 'react';
import {Link} from "react-router-dom";
import playstore from '../images/play_s.svg';
import appstore from '../images/app_s.svg';

const Footer = ()=>{
    return (
        <footer>
           <div className="footer_grid">
                <div className="footer_item">
                    <h4>Company</h4>
                    <ul>
                        <li>
                            <Link to="/">Home</Link>
                        </li>
                        <li>
                            <Link to="/partner">Partner</Link>
                        </li>
                        <li>
                            <Link to="/about-us">About</Link>
                        </li>
                        <li>
                            <Link to="/contact">Contact</Link>
                        </li>
                    </ul>
                </div>
                <div className="footer_item">
                    <h4>Legal</h4>
                    <ul>
                        <li>
                            <Link to="/">Privacy policy</Link>
                        </li>
                        <li>
                            <Link to="/partner">Disclaimer</Link>
                        </li>
                        <li>
                            <Link to="/contact">Terms</Link>
                        </li>
                    </ul>
                </div>
                <div className="footer_item">
                    <h4>Follow us</h4>
                    <ul>
                        <li>
                            <a href="https://facebook.com" target="_blank" rel="noopener noreferrer"> 
                                <svg width="25" height="26" viewBox="0 0 25 26" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M23.62 0H1.38002C1.01406 0.000152852 0.663135 0.150473 0.404363 0.417923C0.14559 0.685373 0.000147892 1.04807 0 1.4263V24.4121C0.000147892 24.7903 0.14559 25.153 0.404363 25.4205C0.663135 25.6879 1.01406 25.8382 1.38002 25.8384H12.5V15.6876H9.51172V11.9964H12.5V9.05497C12.5 5.71789 14.7394 3.90113 17.6239 3.90113C19.0039 3.90113 20.4883 4.00841 20.8331 4.0557V7.541H18.5352C16.966 7.541 16.6669 8.30808 16.6669 9.43851V11.9964H20.4057L19.9174 15.6876H16.6669V25.8384H23.62C23.9859 25.8382 24.3369 25.6879 24.5956 25.4205C24.8544 25.153 24.9999 24.7903 25 24.4121V1.4263C24.9999 1.04807 24.8544 0.685373 24.5956 0.417923C24.3369 0.150473 23.9859 0.000152852 23.62 0Z" fill="black"/>
                                </svg>
                            </a>
                        </li>
                        <li>
                            <a href="https://instagram.com" target="_blank" rel="noopener noreferrer"> 
                                <svg width="25" height="26" viewBox="0 0 25 26" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M17.7081 2.15301C19.0883 2.15727 20.4108 2.72581 21.3867 3.73447C22.3626 4.74312 22.9127 6.10993 22.9169 7.53639V18.302C22.9127 19.7284 22.3626 21.0953 21.3867 22.1039C20.4108 23.1126 19.0883 23.6811 17.7081 23.6854H7.29185C5.91169 23.6811 4.58922 23.1126 3.61329 22.1039C2.63737 21.0953 2.08727 19.7284 2.08315 18.302V7.53639C2.08727 6.10993 2.63737 4.74312 3.61329 3.73447C4.58922 2.72581 5.91169 2.15727 7.29185 2.15301H17.7081ZM17.7081 0H7.29185C3.28125 0 0 3.39129 0 7.53639V18.302C0 22.4471 3.28125 25.8384 7.29185 25.8384H17.7081C21.7188 25.8384 25 22.4471 25 18.302V7.53639C25 3.39129 21.7188 0 17.7081 0Z" fill="black"/>
                                    <path d="M19.2705 7.53637C18.9615 7.53637 18.6594 7.44166 18.4024 7.26422C18.1455 7.08678 17.9452 6.83457 17.8269 6.53949C17.7087 6.24441 17.6777 5.91972 17.738 5.60646C17.7983 5.29321 17.9471 5.00547 18.1657 4.77962C18.3842 4.55378 18.6626 4.39998 18.9657 4.33767C19.2688 4.27536 19.5829 4.30734 19.8684 4.42957C20.154 4.55179 20.398 4.75877 20.5697 5.02434C20.7414 5.2899 20.833 5.60212 20.833 5.92151C20.8334 6.1337 20.7933 6.3439 20.715 6.54003C20.6366 6.73616 20.5215 6.91436 20.3763 7.06441C20.2312 7.21445 20.0587 7.33338 19.869 7.41438C19.6792 7.49537 19.4758 7.53683 19.2705 7.53637Z" fill="black"/>
                                    <path d="M12.5 8.61268C13.3241 8.61268 14.1297 8.86525 14.815 9.33846C15.5002 9.81166 16.0343 10.4842 16.3497 11.2712C16.665 12.0581 16.7476 12.924 16.5868 13.7593C16.426 14.5947 16.0292 15.3621 15.4464 15.9643C14.8637 16.5666 14.1212 16.9768 13.3129 17.1429C12.5046 17.3091 11.6668 17.2238 10.9054 16.8979C10.144 16.5719 9.49325 16.02 9.03539 15.3118C8.57753 14.6036 8.33315 13.7709 8.33315 12.9192C8.33433 11.7774 8.77372 10.6827 9.5549 9.87538C10.3361 9.06801 11.3952 8.6139 12.5 8.61268ZM12.5 6.45972C11.2639 6.45972 10.0555 6.83856 9.02769 7.54833C7.99988 8.25811 7.1988 9.26694 6.72576 10.4473C6.25271 11.6276 6.12894 12.9264 6.37009 14.1794C6.61125 15.4324 7.20651 16.5833 8.08059 17.4867C8.95466 18.3901 10.0683 19.0053 11.2807 19.2545C12.4931 19.5038 13.7497 19.3759 14.8918 18.887C16.0338 18.3981 17.0099 17.5701 17.6967 16.5079C18.3834 15.4456 18.75 14.1968 18.75 12.9192C18.75 11.206 18.0915 9.56304 16.9194 8.35165C15.7473 7.14027 14.1576 6.45972 12.5 6.45972Z" fill="black"/>
                                </svg>

                            </a>
                        </li>
                        <li>
                            <a href="https://twitter.com" target="_blank" rel="noopener noreferrer"> 
                                <svg width="31" height="26" viewBox="0 0 31 26" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M31 3.06158C29.8371 3.588 28.6068 3.93482 27.3478 4.09108C28.6709 3.28388 29.6659 2.00151 30.1462 0.484479C28.8953 1.24657 27.529 1.78129 26.1059 2.06573C25.5066 1.41138 24.7859 0.89083 23.9875 0.535715C23.1891 0.180601 22.3298 -0.0016602 21.4617 1.13947e-05C17.9471 1.13947e-05 15.1028 2.92028 15.1028 6.52014C15.1003 7.02087 15.1554 7.52011 15.2669 8.00719C12.7466 7.8841 10.2788 7.21427 8.02124 6.04056C5.7637 4.86685 3.76617 3.2151 2.15644 1.191C1.5917 2.18289 1.29304 3.31461 1.29167 4.46788C1.29167 6.72873 2.42381 8.72717 4.13333 9.89796C3.12049 9.87292 2.1284 9.59362 1.24129 9.08379V9.16453C1.24129 12.327 3.43712 14.958 6.34337 15.5568C5.79686 15.7086 5.23368 15.7855 4.66808 15.7856C4.26675 15.7863 3.86633 15.7458 3.47265 15.6645C4.28058 18.255 6.63206 20.1391 9.41754 20.1929C7.15414 22.0103 4.37527 22.9926 1.51771 22.9853C1.01048 22.9845 0.503728 22.9531 0 22.8911C2.90701 24.8251 6.28618 25.8481 9.73529 25.8383C21.4481 25.8383 27.847 15.9 27.847 7.28049C27.847 6.99788 27.8399 6.71528 27.827 6.4394C29.069 5.51895 30.1435 4.3751 31 3.06158Z" fill="black"/>
                                </svg>
                            </a>
                        </li>
                    </ul>
                </div>
                <div className="footer_item">
                    <h4>Download Now!</h4>
                    <div className="app_link">
                        <a href="https://play.google.com/store/apps?hl=en" target="_blank" rel="noopener noreferrer"> 
                            <img src={playstore} alt="Playstore's logo"/>
                        </a>
                        <a href="https://www.apple.com/ng/ios/app-store/" target="_blank" rel="noopener noreferrer">
                            <img src={appstore} alt="Appstore's logo"/>
                        </a>
                    </div>
                </div>
           </div>
           <p>&#x24B8; &nbsp; 2020 Meathub Ltd. All rights reserved.</p>
        </footer>
    )
}
export default Footer;