import React from 'react';
import Footer from './Footer';
import Navbar from './Navbar';
import heroMeat from '../images/hero_meat.png'
import playstore from '../images/play_s.svg';
import appstore from '../images/app_s.svg';
import meat1 from '../images/meat1.png';
import meat2 from '../images/meat2.png';
import meat3 from '../images/meat3.png';

const About = ()=>{
    // const loadSpinner = <span className="spinner spinner-blue"></span>;

    return(
        <div>
            <Navbar/>
            <div className="about">
                <section className="hero_bg">
                    <div className="about_hero_grid">
                        <div className="hero_title">
                            <h4>About us.</h4>
                            <p>Who are we.</p>
                        </div>
                        <div className="img_relative">
                            <img src={heroMeat} alt=" meat"/>
                        </div>
                    </div>
                </section>
                <section className="section_container hero_decription_">
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc sapien in quisque amet lectus eu et phasellus. 
                            Vestibulum tristique sagittis adipiscing blandit sit sociis aliquam. Et mollis nisi viverra id nisl, suspendisse. Posuere
                            pellentesque posuere ac tristique mattis gravida. Ligula luctus risus in sagittis donec in risus facilisi. Volutpat duis
                            nibh eget molestie et commodo eget sagittis. Nibh vitae in eget cursus in. Sed fusce in lacus tristique aliquam,
                            ut proin adipiscing interdum. Ornare augue cursus libero pellentesque enim. Egestas tellus ultrices habitant
                            malesuada dolor proin vitae nibh lobortis. Risus ut scelerisque faucibus tincidunt viverra. Eu, velit orci vitae
                            dignissim tristique ut lacus platea commodo. Malesuada blandit purus pulvinar scelerisque nunc convallis quis
                            ipsum. Phasellus tempus aenean sed lectus adipiscing eros, sem pulvinar malesuada. Elit orci amet fermentum lacus
                            montes, vivamus. Magna tortor est risus, non cursus et malesuada. Sagittis, semper arcu pellentesque sapien. Viverrasem fames aliquet in donec potenti ipsum ornare accumsan. Sagittis, venenatis, volutpat consectetur blandit potenti consequat erat.</p>
                </section>
                <section className="mission_bg">
                    <div className="section_container">
                        <h4>The Meathub Mission</h4>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Leo suspendisse viverra vulputate eros proin ornare libero, ultricies venenatis, orci massa, convallis iaculis praesent. Lacus sapien malesuada ante vel duis fusce dui.</p>
                    </div>
                </section>
                <section className=" process_bg">
                    <div className="section_container">
                    <div className="process_container">
                        <h4>The Meathub process</h4>
                        <div className="process_body">
                            <div className="process_item">
                                <div className="item_container">
                                    <h4>Lorem ipsum dolor</h4>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Accumsan neque metus eleifend cursus. Eros, ullamcorper mi ultricies tellus interdum ipsum eu, amet risus. </p>
                                </div>
                            </div>
                            <div className="process_item">
                                <div className="item_container">
                                    <h4>Lorem ipsum dolor</h4>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Accumsan neque metus eleifend cursus. Eros, ullamcorper mi ultricies tellus interdum ipsum eu, amet risus. </p>
                                </div>
                            </div>
                            <div className="process_item">
                                <div className="item_container">
                                    <h4>Lorem ipsum dolor</h4>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Accumsan neque metus eleifend cursus. Eros, ullamcorper mi ultricies tellus interdum ipsum eu, amet risus. </p>
                                </div>
                            </div>
                            <div className="process_item">
                                <div className="item_container">
                                    <h4>Lorem ipsum dolor</h4>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Accumsan neque metus eleifend cursus. Eros, ullamcorper mi ultricies tellus interdum ipsum eu, amet risus. </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    </div>
                </section>
                <section className="app_underline">
                    <div className="app_container section_container">
                        <div className="description">
                            <p>Order for meat by first,</p>
                            <p className="download">Downloading the Meathub app on</p>
                        </div>
                        <div className="download_links">
                            <a href="https://play.google.com/store/apps?hl=en" target="_blank" rel="noopener noreferrer"> 
                                <img src={playstore} alt="Playstore's logo"/>
                            </a>
                            <a href="https://www.apple.com/ng/ios/app-store/" target="_blank" rel="noopener noreferrer">
                                <img src={appstore} alt="Appstore's logo"/>
                            </a>
                        </div>
                    </div>
                </section>
                <section className=" section_container product_section">
                    <div className="product_section_title">
                        <h4>The Meathub Products</h4>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Blandit nunc tortor, vitae eu. Arcu lacus etiam montes, id nulla morbi. Ipsum et dui integer id. Et eu aliquet lectus cursus vestibulum in velit. Id morbi scelerisque semper ullamcorper leo mattis neque, et.</p>
                    </div>
                    <div className="product_lists">
                        <div className="product">
                            <img src={meat1} alt="Product"/>
                            <h4>5 portion share</h4>
                            <p>Lorem ipsum dolor sit amet, consecteturadipiscing elit. Accumsan neque metus eleifend cursus. Eros, ullamcorper mi ultricies tellus interdum ipsum eu, amet risus. </p>
                        </div>

                        <div className="product">
                            <img src={meat2} alt="Product"/>
                            <h4>10 portion share</h4>
                            <p>Lorem ipsum dolor sit amet, consecteturadipiscing elit. Accumsan neque metus eleifend cursus. Eros, ullamcorper mi ultricies tellus interdum ipsum eu, amet risus. </p>
                        </div>
                        <div className="product">
                            <img src={meat3} alt="Product"/>
                            <h4>15 portion share</h4>
                            <p>Lorem ipsum dolor sit amet, consecteturadipiscing elit. Accumsan neque metus eleifend cursus. Eros, ullamcorper mi ultricies tellus interdum ipsum eu, amet risus. </p>
                        </div>

                    </div>
                </section>
            </div>
            <Footer/>
        </div>
    )
}
export default About;