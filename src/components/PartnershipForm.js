import React,{useState} from 'react';
import emailjs from 'emailjs-com';


function Form (props){
    const [spin_loader, setSpin_loader] = useState(false);
    const [ name, setName] = useState("");
    const [ tel, setTel] = useState("");
    const [ telErr, setTelErr] = useState("");
    const [ email, setEmail] = useState("");
    const [ outlet, setOutlet] = useState("");
    const [ inputError, setInputError] = useState("");
    // spinner 
    const loadSpinner = <span className="spin_loader spin_loader-blue"></span>;

    //validation of form
    const handleName= (e)=> setName(e.target.value)
    const handleTel= (e)=> {
        // validate phone format 
        let phoneFormat = /^\d{11}$/;
        if(e.target.value.match(phoneFormat)){
            setTel(e.target.value);
            setTelErr("");
        }else{
            setTelErr("Invalid phone number")
        }
    }
    const handleEmail= (e)=> setEmail(e.target.value)
    const handleOutlet= (e)=> setOutlet(e.target.value)

    const handleSubmit = (e)=>{
        e.preventDefault();
        setSpin_loader(true);
        if(name && tel && email && outlet !== ""){
            emailjs.sendForm('meathubng', 'meathub_contact_form', e.target, 'user_rVpgGKz8jOSlnZWm1ImuS')
            .then((result) => {
                props.history.push("/partner/success")
            }, (error) => {
                console.log(error);
                setInputError("Something went wrong, please try again later")
                setSpin_loader(false);

            });
        }else{
            setInputError("Please fill the required field(s)")
            setSpin_loader(false);
        }
    }
    return(
        <form id="man" onSubmit={handleSubmit}>
            <div className="form_header">
                <h4>Be a part of the Franchise</h4>
                <p>Partner with us and become a part of the franchise. Fill the form below;</p>
                <span className="meathub_form_err">{inputError}</span>
            </div>
            <span className="form_group">
                <input type="text" placeholder="Enter your name" name="from_name" onChange={handleName}/>
            </span>
            <span className="meathub_form_err">{telErr}</span>
            <span className="form_group">
                <input type="tel" name="phone_number" placeholder="Enter your phone eg 09088432451" onBlur={handleTel}/>
            </span>
            <span className="form_group">
                <input type="email" name ="user_email" placeholder="Enter your email" onChange={handleEmail}/>
            </span>
            <span className="form_group">
                <input type="number" name="outlets" min="1" placeholder="How many outlets do you want to partner " onChange={handleOutlet}/>
            </span>
            <span className="form_group">
                <textarea name="text" placeholder="This is optional "></textarea>
            </span>
            {/* <Input.Password placeholder="Enter your password"/> */}
            <button type="submit">Become a Partner 
                {spin_loader ? loadSpinner : ""}
            </button>
        </form>
    )
}
export default Form;