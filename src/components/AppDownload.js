import React from 'react';
import playstore from '../images/play_s.svg';
import appstore from '../images/app_s.svg';

const AppDownload = ()=>{
    return(
        <div className="download_app">
            <h2>Download the Meathub app</h2>
            <p>Start placing your orders.</p>
            <div className="app_link">
                <a href="https://play.google.com/store/apps?hl=en" target="_blank" rel="noopener noreferrer"> 
                    <img src={playstore} alt="Playstore's logo"/>
                </a>
                <a href="https://www.apple.com/ng/ios/app-store/" target="_blank" rel="noopener noreferrer">
                    <img src={appstore} alt="Appstore's logo"/>
                </a>
            </div>
        </div>
    )
}

export default AppDownload;