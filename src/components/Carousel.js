import React from 'react';
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import why_1 from '../images/why_1.svg';
import why_2 from '../images/why_2.svg';
import why_3 from '../images/why_3.svg';
import why_4 from '../images/why_4.svg';
import why_5 from '../images/why_5.svg';

const Carousel = ()=>{
    var settings = {
        dots: true,
        infinite: true,
        speed: 500,
        slidesToShow: 3,
        slidesToScroll: 1,
        autoplay: true,
      autoplaySpeed: 3000,
      responsive: [
        {
          breakpoint: 992,
          settings: {
            slidesToShow: 2,
          }
        },
        {
          breakpoint: 554, 
          settings: {
            slidesToShow: 1
          }
        }
      ]
        
      };
    return(
        <Slider {...settings} className="_slider">
          <div className="slider_item_cont">
            <div className="slider_item">
                <img src={why_1} alt="why meathub description icon"/>
                <h4>Affordable</h4>
                <p>Our products comes in different packages which are all priced reasonably and we ensure that you receive value for your money. Save money and eat delicious, healthy meat.</p>
            </div>
          </div>
          <div className="slider_item_cont">
            <div className="slider_item">
                <img src={why_2} alt="why meathub description icon"/>
                <h4>Hygienic</h4>
                <p>Our meat uses zero preservatives and are 100% Healthy. They are prepared through the best practices and our processing facility is Halal compliant.</p>
            </div>
          </div>
          <div className="slider_item_cont">
            <div className="slider_item">
                <img src={why_3} alt="why meathub description icon"/>
                <h4>Certified</h4>
                <p>Our products is certified by Lagos State Government and we are holders of the Veterinary Health Certificate for Trade of Meat. This means our meat is processed under hygienic conditions, tested and marked for consumption. </p>
            </div>
          </div>
          <div className="slider_item_cont">
            <div className="slider_item">
                <img src={why_4} alt="why meathub description icon"/>
                <h4>Traceable</h4>
                <p>We keep track of the cattle we slaughter by incorporating RFID (Radio Frequency Identification) technology. These tags use radio waves to read and capture information on the location of the cattle throughout processing.</p>
            </div>
          </div>
          <div className="slider_item_cont">
            <div className="slider_item">
                <img src={why_5} alt="why meathub description icon"/>
                <h4>Fit for Slaughter</h4>
                <p>We ensure that our cattle meet the appropriate weight, health, and requirements for slaughter and consumption through examination by veterinary experts.</p>
            </div>
          </div>
        </Slider>
    )
}

export default Carousel;