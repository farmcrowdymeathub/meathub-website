import React from 'react';
// import Navbar from "./components/Navbar";
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import 'antd/dist/antd.css';
import './sass/meathub.scss';
import Home from './components/Home';
// import Footer from './components/Footer';
import Partner from './components/Partner';
import Error404 from './components/errorPages/Error404';
import About from './components/About';
import RegSuccess from './components/RegSuccessful';


function App() {
  return (
    <Router>
      <div className="App">
      <Switch>
          <Route exact path="/" component={Home}/>
          <Route exact path="/partner" component={Partner}/>
          <Route exact path="/about-us" component={About}/>
          <Route exact path="/partner/success" component={RegSuccess}/>
          <Route component={Error404}/>
        </Switch>
      </div>
    </Router>
  );
} 
 
export default App;
 